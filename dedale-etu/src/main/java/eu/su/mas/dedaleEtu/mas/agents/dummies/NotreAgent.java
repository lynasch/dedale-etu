package eu.su.mas.dedaleEtu.mas.agents.dummies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.DFBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.MainChasseBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.MainExploBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;


public class NotreAgent extends AbstractDedaleAgent {

	private static final long serialVersionUID = -6431752665590433727L;
	private MapRepresentation myMap;
	private ArrayList<String> agentList = new ArrayList<String>();
	private String lastPos = "";
	private int mode = 0;
	/**
	 * Mode : 0 -> Exploration
	 * 		  1 -> Chasse
	 */
	private int blockedState = 0;
	private AID otherAgent;
	private String nextNode;


	protected void setup(){

		super.setup();

		List<Behaviour> lb=new ArrayList<Behaviour>();

		/*
		 * Inscrit l'agent dans les Pages Jaunes et lui permet de récupérer la liste des autres agents de la plateforme.
		 */
		lb.add(new DFBehaviour(this));
		/*
		 * Si on est en mode exploration, on active MainExploBehaviour
		 * Si on est en mode chasse, on active MainChasseBehaviour
		 */
		if (this.mode == 0) {
			System.out.println("Séléction du mode Exploration");
		lb.add(new MainExploBehaviour(this));
		}if (this.mode == 1) {
			System.out.println("Séléction du mode Chasse");
			lb.add(new MainChasseBehaviour(this));
		}
		
		
		addBehaviour(new startMyBehaviours(this,lb));
		
		System.out.println("the agent "+this.getLocalName()+ " is started");

	}
	

	
	//Récupère la liste des agents de la plateforme
	public ArrayList<String> getAgentList() {
		return this.agentList;
	}
	
	
	//Configure la liste des agents de la plateforme
	public void setAgentList(ArrayList<AID> agentList) {
		// TODO Auto-generated method stub
		for (AID agent :agentList) {
			this.agentList.add(agent.getLocalName());
		}
	}
	
	// Récupère la dernière position avant déplacement
	public String getLastPos() {
		return lastPos;
	}
	
	// Configure la dernière position avant déplacement
	public void setLastPos(String lastPos) {
		this.lastPos = lastPos;
	}

	//Récupère la carte des connaissances de l'agent
	public MapRepresentation getMap() {
		return myMap;
	}
	
	public void setMap(MapRepresentation myMap) {
		this.myMap = myMap;
	}
	
	
	public int getMode() {
		return this.mode;
	}

	public int getBlockedState() {
		return this.blockedState ;
	}
	
	public void setBlockedState(int blockedState) {
		this.blockedState = blockedState;
	}

	public AID getOtherAgent() {
		return otherAgent;
	}	

	public void setOtherAgent(AID otherAgent) {
		this.otherAgent = otherAgent;
	}

	public void setNextNode(String nextNode) {
		this.nextNode = nextNode;
		
	}



	public String getNextNode() {
		return nextNode;
	}

}
