package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
//import mas.agents.NotreAgent;

public class DFBehaviour extends OneShotBehaviour{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5682210724325125967L;
	/**
	 * Ce comportement est appelé une fois à la création de l'agent afin de l'enregistrer dans les pages jaunes
	 * Une fois inscrit dans les pages jaunes, il attends un petit délai avant de récupérer la liste des agents inscrits sur la plateforme.
	 */
	
	public DFBehaviour(final AbstractDedaleAgent myagent) {
		super(myagent);
	}
	
	
	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " : DFBEHAVIOUR.");
		
		// On inscrit l'agent dans les pages jaunes DF
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.myAgent.getAID()); 
		ServiceDescription sd  = new ServiceDescription();
		sd.setName(this.myAgent.getLocalName() );
		sd.setType( "explore_chasse" );
		dfd.addServices(sd);

		try {
			DFService.register(this.myAgent, dfd );
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		//il faut que tout le monde soit inscrit avant de recuperer les agents
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
				
		// on récupère la liste des agents de la plateforme.
		// On n'a qu'un seul comportement donc pas besoin de chercher tout les services.
		try {
			DFAgentDescription[] result = DFService.search(this.myAgent, new DFAgentDescription());
			ArrayList<AID> agentList = new ArrayList<AID>();
			for (DFAgentDescription ad : result){
				if(!ad.getName().equals(this.myAgent.getAID()))
					agentList.add(ad.getName());
				
			}
			((NotreAgent)this.myAgent).setAgentList(agentList);	
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}	
	}
}
