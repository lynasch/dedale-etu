package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareMapBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;


/**
 * <pre>
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs. 
 * This (non optimal) behaviour is done until all nodes are explored. 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.
 * Warning, the sub-behaviour ShareMap periodically share the whole map
 * </pre>
 * @author hc
 *
 */
public class ExploreBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;
	private int step = 0;
	private final int MAX_STEP = 10;
	private int nextBehaviour = 0;
	
	/**
	 * next_behaviour : 1 -> SharedMap
	 * 					2 -> Blocked
	 * 					3 -> End
	 */

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	private List<String> agentList = new ArrayList<String>();

	public ExploreBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap) {
		super(myagent);
		this.myMap=myMap;
		
		
		
	}

	@Override
	public void action() {
		
		System.out.println(this.myAgent.getLocalName() + " : EXPLOREBEHAVIOUR.");
		
		if(this.myMap==null) {
			
			this.myMap= new MapRepresentation();
		}
		

		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//1) remove the current node from openlist and add it to closedNodes.
			this.myMap.addNode(myPosition, MapAttribute.closed);

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;
			Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
			while(iter.hasNext()){
				String nodeId=iter.next().getLeft();
				boolean isNewNode=this.myMap.addNewNode(nodeId);
				//the node may exist, but not necessarily the edge
				if (myPosition!=nodeId) {
					this.myMap.addEdge(myPosition, nodeId);
					if (nextNode==null && isNewNode) nextNode=nodeId;
				}
			}
			

			//3) while openNodes is not empty, continues.
			if (!this.myMap.hasOpenNode()){
				//Explo finished
				nextBehaviour = 3;
				finished=true;
				System.out.println(this.myAgent.getLocalName()+" - Exploration successufully done, behaviour removed.");
			}else{
				//4) select next move.
				//4.1 If there exist one open node directly reachable, go for it,
				//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
				if (nextNode==null){
					//no directly accessible openNode
					//chose one, compute the path and take the first step.
					nextNode=this.myMap.getShortestPathToClosestOpenNode(myPosition).get(0);
					((NotreAgent)this.myAgent).setNextNode(nextNode);
				}
				
				/*
				 * BLOCKEDBEHAVIOUR
				 */
				//System.out.println(((NotreAgent)this.myAgent).getLocalName() + "My Position : " + myPosition + "Mon ancienne pos : " + ((NotreAgent)this.myAgent).getLastPos());
				if(((NotreAgent)this.myAgent).getLastPos().equals(myPosition) && step >  1) {
					
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.setProtocol("BLOCKED");
					msg.setSender(this.myAgent.getAID());
					for (String agentName : ((NotreAgent)this.myAgent).getAgentList()) {
						msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
						
					}
					try {					
						//msg.setContentObject(nextNode + "+" + ((AbstractDedaleAgent)this.myAgent).getCurrentPosition());
						msg.setContentObject("Je suis bloqué !");
					} catch (IOException e) {
						e.printStackTrace();
					}
					((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
					
					System.out.println(this.myAgent.getLocalName() + " : Qui est bloqué avec moi ? : " + nextNode + "+" + ((AbstractDedaleAgent)this.myAgent).getCurrentPosition());
					block(1000);
					
					
					((NotreAgent)this.myAgent).setBlockedState(1);
					((NotreAgent)this.myAgent).setMap(this.myMap);
					nextBehaviour = 2;
					finished = true;
				}
				
				//mise a jour de la position
				((NotreAgent)this.myAgent).setLastPos(myPosition);
				
				/*
				 * SHAREMAPBEHAVIOUR
				 */
				if (step >= MAX_STEP) {
					step = 0;
					((NotreAgent)this.myAgent).setMap(this.myMap);
					this.myAgent.addBehaviour(new ShareMapBehaviour(this.myAgent,this.myMap ));
					//nextBehaviour = 1;
					finished = true;
				}
				step++;
				((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
			}
		}
	}
	
	public int onEnd(){
         return nextBehaviour;
     }

	@Override
	public boolean done() {
		return finished;
	}

}
