package eu.su.mas.dedaleEtu.mas.behaviours;

import jade.core.behaviours.OneShotBehaviour;

public class FinalBehaviour extends OneShotBehaviour{

	/**
	 * fin de la phase d'exploration ou de chasse
	 */
	private static final long serialVersionUID = -4963831358497269726L;

	@Override
	public void action() {	
		System.out.println(this.myAgent.getLocalName() + " : FINALBEHAVIOUR.");
	}

}
