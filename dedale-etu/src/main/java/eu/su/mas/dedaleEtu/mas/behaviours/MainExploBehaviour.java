package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import jade.core.behaviours.FSMBehaviour;


public class MainExploBehaviour extends FSMBehaviour{

	/***
	 * Comportement principal de notre agent.
	 * On commence avec une Exploration. Tout les 10 pas, les agents partagent leur carte avec le reste des autres agents.
	 * Lorsqu'ils sont coincés, ils passent en interblocage et se débloquent. Lorsque c'est bon, il reprennent avec l'exploration.
	 * Une fois toute la carte visité, ils finissent.
	 */
	
	private static final long serialVersionUID = 1L;
	private MapRepresentation myMap;
	
	public MainExploBehaviour(final AbstractDedaleAgent myagent) {
		super(myagent);
		this.myMap = ((NotreAgent)this.myAgent).getMap();
		
		//STATES
		registerFirstState(new ExploreBehaviour(myagent, this.myMap), "Exploration");
		registerState(new BlockedBehaviour(myagent, this.myMap), "Bloqué");
		//registerState(new ShareMapBehaviour(myagent, this.myMap), "Partage de Carte");
		registerLastState(new FinalBehaviour(), "Fin");
		
		//TRANSITIONS
		registerDefaultTransition("Exploration", "Exploration");
		//registerTransition("Exploration", "Partage de Carte", 1);
		registerTransition("Exploration", "Bloqué", 2);
		registerTransition("Exploration", "END", 3);
		
		//registerDefaultTransition("Partage de Carte", "Exploration");
		//registerTransition("Partage de Carte", "Bloqué", 1);
		
		registerDefaultTransition("Bloqué", "Bloqué");
		registerTransition("Bloqué", "Exploration", 1);
		;


		
	}
	

}
