package eu.su.mas.dedaleEtu.mas.behaviours;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

//import mas.agents.Data;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;

public class BlockedBehaviour extends SimpleBehaviour{


	private static final long serialVersionUID = -8367108622771906827L;
	private AID agent ; 	

	

	private MapRepresentation myMap;
	private boolean finished;
	private int waitingTime = 0;
	private AID otherAgent;
	private int nextBehaviour = 0;

	
	public BlockedBehaviour (final AbstractDedaleAgent myagent, MapRepresentation myMap) {
		super(myagent);
		this.myMap = myMap;
	}
	
	/*
	 * state 1 : On attends un message d'un agent bloqué puis on passe en 1
	 * state 2 : on echange les maps avec l'agent bloqué 
	 * state 3 : si le conflit est réglé, on sort du comportement, sinon on passe en 3
	 * state 4 : si on peut revenir au noeud precedent on recule. sinon, on attends 2000 secondes.
	 * 			 si doit bouger -> state 4
	 * 			 sinon state 5
	 * state 5 : on doit bouger : on fait demi tour et on retourne en Exploration
	 * 			 On remet le state à 0 de l'autre agent aussi
	 * state 6 : on attends jusqu'a ce que l'état de blocage de l'autre soit à 0 et on retourne en Exploration
	 */
	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " : BLOCKEDBEHAVIOUR.");
		int state = ((NotreAgent)this.myAgent).getBlockedState();
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		if (this.myMap == null) {
			this.myMap = ((NotreAgent)this.myAgent).getMap();
		}

		
		switch(state) {
			case 1:
				System.out.println(this.myAgent.getLocalName() + " : BLOCKEDstate 1");
				
				
				String nextNode = ((NotreAgent)this.myAgent).getNextNode();
				
				
				
				/*
				 * on attends le message de l'agent avec qui on est en interblocage
				 */
				block(500);
				MessageTemplate msgTemplate=MessageTemplate.and(
						MessageTemplate.MatchProtocol("BLOCKED"),
						MessageTemplate.MatchPerformative(ACLMessage.INFORM));		
				ACLMessage answer = this.myAgent.receive(msgTemplate);
				
				
				
				
				/*
				 *  le message contiendra notre position+la position de l'autre
				 */
				//if(answer != null && answer.getContent().equals(((AbstractDedaleAgent)this.myAgent).getCurrentPosition()+"+"+nextNode )){
				if(answer != null && answer.getContent().equals("Je suis bloqué !")){
					otherAgent =  answer.getSender();
					((NotreAgent)this.myAgent).setOtherAgent(answer.getSender()); 
					((NotreAgent)this.myAgent).setBlockedState(state+1);
					System.out.println(this.myAgent.getLocalName() + " : Je suis bloqué avec ." + otherAgent.getLocalName());
				} else {
					answer = null;
					//block(100);
					waitingTime ++;
				}
				/*
				 * si temps d'attente trop long on revient à Explore
				 */
				if(waitingTime == 100  && answer==null){
					System.out.println(this.myAgent.getLocalName()+" : Je quitte l'interblocage");
					nextBehaviour = 1;
					finished = true;
				}
				break;
			case 2:
				/*
				 * on échange les graphes avec l'agent bloqué
				 */
				System.out.println(this.myAgent.getLocalName() + " : BLOCKEDstate 2");
				
				//nextBehaviour  = 2;
				this.myAgent.addBehaviour(new ShareMapBehaviour(this.myAgent,this.myMap ));
				((NotreAgent)this.myAgent).setBlockedState(state+1);
				finished = true;
				break;
			case 3:
				
				System.out.println(this.myAgent.getLocalName() + " : BLOCKEDstate 3");
				((NotreAgent)this.myAgent).setBlockedState(0);
				break;
			case 4:
				break;
			case 5:
				break;
			case 0:
				System.out.println(this.myAgent.getLocalName() + " : BLOCKEDstate 0");
				nextBehaviour = 1;
				finished = true;
				break;	
		}
	}
	
	
	@Override
	public boolean done() {
		return finished;
	}

}
