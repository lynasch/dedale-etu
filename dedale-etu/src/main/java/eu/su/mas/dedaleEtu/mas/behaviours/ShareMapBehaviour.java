package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.QueryAgentsOnLocation;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)  	
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class ShareMapBehaviour extends SimpleBehaviour{
	/**
	 * 
	 */
	private static final long serialVersionUID = -568863390879327961L;
	//private boolean finished = false;
	
	private MapRepresentation myMap;
	private List<String> agentList = new ArrayList<String>();
	private int nextBehaviour = 0;
	/**
	 * next_behaviour : 0 -> Explore 
	 * 					1 -> Blocked
	 */
	private boolean finished;


	/**
	 * The agent periodically share its map.
	 * It blindly tries to send all its graph to its friend(s)  	
	 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

	 * @param a the agent
	 * @param period the periodicity of the behaviour (in ms)
	 * @param mymap (the map to share)
	 * @param receivers the list of agents to send the map to
	 */
	public ShareMapBehaviour(Agent a, MapRepresentation mymap) {
		super(a);
		this.myMap=mymap;	
		
		// on récupère la liste des receivers dans agentList

		if (((NotreAgent)this.myAgent).getBlockedState() == 2) {
			this.agentList.add(((NotreAgent)this.myAgent).getOtherAgent().getLocalName());
		}else {
			this.agentList = ((NotreAgent)this.myAgent).getAgentList();
		}
		System.out.println(agentList);
		

	}




	
	public void action() {
		System.out.println(this.myAgent.getLocalName() + " : SHAREMAPBEHAVIOUR.");

		if (this.myMap == null) {
			this.myMap = ((NotreAgent)this.myAgent).getMap();
		}
		
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("SHARE-TOPO");
		msg.setSender(this.myAgent.getAID());
		for (String agentName : agentList) {
			msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
			System.out.println(this.myAgent.getLocalName() + " : J'envoie ma carte à " + agentList );
		}
		
			
		SerializableSimpleGraph<String, MapAttribute> sg=this.myMap.getSerializableGraph();
		try {					
			msg.setContentObject(sg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		
		
		block(2000);
		
		//5) At each time step, the agent check if he received a graph from a teammate. 	
		// If it was written properly, this sharing action should be in a dedicated behaviour set.
		MessageTemplate msgTemplate=MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		//Takes the message if the template is verified
		ACLMessage msgReceived=this.myAgent.receive(msgTemplate);
		if (msgReceived!=null) {
			SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
			try {
				sgreceived = (SerializableSimpleGraph<String, MapAttribute>)msgReceived.getContentObject();
				System.out.println(agentList.get(0) + " : J'ai reçu une carte. ");
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.myMap.mergeMap(sgreceived);
			((NotreAgent)this.myAgent).setMap(this.myMap);
			finished = true;
		}else {
			block(3000);
		}
	}


	public int onEnd(){
        return nextBehaviour;
    }

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}


}
