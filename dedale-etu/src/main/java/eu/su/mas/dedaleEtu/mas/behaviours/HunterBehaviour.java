package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;

public class HunterBehaviour extends SimpleBehaviour {
	
	private MapRepresentation myMap;
	private int nextBehaviour = 0;
	private boolean finished = false;
	private ArrayList<String> list_agentNames = new ArrayList<String>();
	private String OwnInsideStench;
	
	
	
	public HunterBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap) {
		super(myagent);
		this.myMap = myMap;
		
		// on récupère la liste des receivers dans les pages jaunes
		ArrayList<AID> agentList = ((NotreAgent)this.myAgent).getAgentList();
		for (AID agent : agentList) {
			this.list_agentNames.add(agent.getLocalName());
	    }
		System.out.println("agentnames : " + this.list_agentNames);
	}
	
	
	@Override
	public void action() {
		System.out.println(this.myAgent.getLocalName() + ": HunterBehaviour.");
		
	
		finished  = true;

	}

	public int onEnd(){
        return nextBehaviour;
    }
	
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}

}
