package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.FSMBehaviour;


public class ChasseBehaviour extends FSMBehaviour{

	/***
	 * Comportement principal de notre agent.
	 * On commence avec une Exploration. Tout les 10 pas, les agents partagent leur carte avec le reste des autres agents.
	 * Lorsqu'ils sont coincés, ils passent en interblocage et se débloquent. Lorsque c'est bon, il reprennent avec l'exploration.
	 * Une fois toute la carte visité, ils finissent.
	 */
	
	private static final long serialVersionUID = 1L;
	private MapRepresentation myMap;
	
	public ChasseBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap) {
		super(myagent);
		this.myMap = myMap;
		
		//STATES
		registerFirstState(new HunterBehaviour(myagent, this.myMap), "Exploration");
		registerState(new BlockedBehaviour(myagent, this.myMap), "Bloqué");
		registerState(new ShareMapBehaviour(myagent, this.myMap), "Partage de Cartes");
		
		registerLastState(new FinalBehaviour(), "Fin");
		
		//TRANSITIONS
		registerDefaultTransition("Chasse", "Chasse");
		registerTransition("Chasse", "Partage de Cartes", 2);
		registerTransition("Chasse", "Bloqué", 3);
		registerTransition("Chasse", "END", 4);

		
		registerDefaultTransition("Partage de Cartes", "Chasse");
		
		registerDefaultTransition("Bloqué", "Chasse");


		
	}
	

}
