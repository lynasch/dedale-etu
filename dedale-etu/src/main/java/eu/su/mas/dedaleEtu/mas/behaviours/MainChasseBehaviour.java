package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.agents.dummies.NotreAgent;
import jade.core.behaviours.FSMBehaviour;


public class MainChasseBehaviour extends FSMBehaviour{

	/***
	 * Comportement principal de notre agent, dans le cas où on est en mode 2 (comportement chasse)
	 * On commence avec une chasse. Tout les 10 pas, les agents partagent leur carte avec le reste des autres agents.
	 * Lorsqu'ils sont coincés, ils passent en interblocage et se débloquent. Lorsque c'est bon, il reprennent avec l'exploration.
	 * Une fois toute tout les wumpus chassés, on finit.
	 */
	
	private static final long serialVersionUID = 1L;
	private MapRepresentation myMap;
	
	public MainChasseBehaviour(final AbstractDedaleAgent myagent) {
		super(myagent);
		this.myMap = ((NotreAgent)this.myAgent).getMap();
		
		//STATES
		registerFirstState(new HunterBehaviour(myagent, this.myMap), "Chasse");
		registerState(new BlockedBehaviour(myagent, this.myMap), "Bloqué");
		registerLastState(new FinalBehaviour(), "Fin");
		
		//TRANSITIONS
		registerDefaultTransition("Chasse", "Exploration");
		registerTransition("Chasse", "Bloqué", 2);
		registerTransition("Chasse", "END", 3);
		
		registerDefaultTransition("Bloqué", "Chasse");
	}
	

}
